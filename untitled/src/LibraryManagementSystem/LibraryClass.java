package LibraryManagementSystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LibraryClass {
    private Map<String,List<BookPojo>> booksByAuthor;

    public void Library(){
        booksByAuthor   = new HashMap<>();

    }

//    add a Book to the library
    public void addBook(BookPojo bookPojo){
        String author= bookPojo.getAuthor();

        if(!booksByAuthor.containsKey(author)){
            booksByAuthor.put(author,new ArrayList<>());
        }
        booksByAuthor.get(author).add(bookPojo);
    }
    // Get books by a specific author

    public List<BookPojo> getBookByAuthor(String author){
//        return booksByAuthor.get(author);
          return booksByAuthor.getOrDefault(author,new ArrayList<>());

    }

}
