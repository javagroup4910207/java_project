public class Pattern {
    char assA=65;
    char ass='A';

    public Pattern() {
    }
    public void pyramidPatten (){
        int num=10;
        for(int i=0; i<num; i++){
            for(int j=i; j<num;j++) {
                System.out.print(" ");
            }
                for(int k=1; k<=i;k++){
                    System.out.print("* ");
            }
            System.out.println();
        }
    }
    public void pyramidRevPatten (){
        int num=5;
        String out="";
        for(int i=0; i<num; i++){
            String ptn="";

            for(int j=0; j<i;j++) {
                ptn+=" ";
            }
            for(int k=i; k<num;k++){
                ptn+="* ";
            }
            out+=ptn+"\n";
        }
        System.out.println(out);
    }
    public void ButterflyStarPattern(){
        int num =5;
        String out = "";
        String star ="*";
        String space=" ";
        for(int i =1;i<num*2;i++){
            for(int j=1;j<=num;j++){
                for(int k=i+j;k<=j*2;k++){
                    out+="* ";
                }
                out+=" ";

            }
            out+="\n";
        }
        System.out.println(out);

    }
    public void numTriangle(){
        int num=6;
        for(int i=0; i<=num; i++){
            int newNum=i;

            for(int j=i; j<=num;j++) {
                System.out.print(" ");
            }
            for(int k=0; k<=i;k++){
                newNum++;
                System.out.print(newNum+" ");

            }

            System.out.println();

        }

    }
    public void hollowSquare(){
        int num=6;
        for(int i=1; i<=num; i++){
            for(int j=1; j<=num; j++){
                if(i==1 || i==num || j==1 || j==num) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
           
        }

        }
    public void hollowTriPattern (){
        int num=10;
        for(int i=0; i<=num; i++){
            for(int j=i; j<num;j++) {
                System.out.print(" ");
            }
            for(int k=1; k<=i;k++){
//                System.out.print("* ");
                if(k==1 ||k==i || i==num ){
                    System.out.print("* ");
                }else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    public void tiltedTriPtn(){
        int num=5;
        for (int i=1; i<2*num; i++){
            for (int j=1; j<= num; j++){
                if((i+j)%2==0 && (i+j)<=2*num && j<=i ){
                    System.out.print(" * ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println(" ");

        }

    }
    public void ButterflyPtn(){
        int num=5;
        for (int i=1; i<2*num; i++){
            for (int j=1; j< 2*num; j++){
                if((i+j)%2==0 && (i+j)<=2*num && j<=i  ){
                    System.out.print(" * ");
                }
                else{
                    System.out.print(" "+" "+" ");
                }
            }
            System.out.println(" ");

        }

    }
    public void wPattern(){
        int num=5;
        for(int i =1; i<=num; i++){
            for(int m=1; m<=3;m++) {
                for (int j = 2; j <= i; j++) {
                    System.out.print(" ");
                }
                for (int k = 1; k <= num - i; k++) {
                    if(k==1 ||k==num-i) {
                        System.out.print("* ");
                    }else{
                        System.out.print("  ");
                    }
                }
                for (int j =2; j <= i; j++) {
                    System.out.print(" ");
                }

            }
            System.out.println("");

        }
    }

    public static void main(String[] args) {
        Pattern obj1 = new Pattern();
//        obj1.ButterflyStarPattern();
//        obj1.hollowSquare();
//        obj1.hollowTriPattern();
//        obj1.ButterflyPtn();
        obj1.wPattern();

    }
}
