public class Typeconversion {
    private  static String newVar ="new word";
    private  String newInput = "non static variable";
    public static  void switchMethod(){
        int day = 1;
        switch (day) {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
            default:
                break;
        }
    }
    public static  void  forLoopMethod(int num){
         String star="*";
         String out ="";
         for(int i=1; i<=num; i++){
             out+="* ";
             System.out.println(out);
         }
        System.out.println();

    }
    public static  void main(String[] args){
        double myDouble =9.56d;
        int myInt = (int) myDouble;
        String[] newStr ={"apple","ball","cat"};
        System.out.println(newStr[2]);
        int[] newInt = {1,2,3,4,5,6,8,8};
        System.out.println(myInt);
        int[][] myNumbers = { {1, 2, 3, 4}, {5, 6, 7} };
        switchMethod();
        forLoopMethod(3);
//    object for main class
        Typeconversion newObject = new Typeconversion();
        System.out.println("newVar =" +newVar);
        System.out.println("newInput "+newObject.newInput);


    }
}
