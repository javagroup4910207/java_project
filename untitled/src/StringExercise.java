import java.util.Arrays;

public class StringExercise {
    public static void main(String[] args) {

        char[] ch ={'g','i','r','i','t','h','a','r','a','n'};
        String str1=new String(ch);
        String str2 ="newString";
        String str3="newString";
        char[] chArr1= str3.toCharArray();
        String out="";

        for (char c : chArr1) {
            out = c + out;

        }
        System.out.println("reverse "+out);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str3);
        stringBuilder.reverse();
        System.out.println(stringBuilder.toString());


//        String Literals are stored in a memory called string pool

        System.out.println(str1);
//       charAt method
        char getCharacter = str2.charAt(3);
        System.out.println(getCharacter);
//        compareTo method
//        it returns integer value
        System.out.println("comparison method "+str2.compareTo(str3));
        System.out.println("comparison method "+str1.compareTo(str2));
        System.out.println("comparison method "+str2.compareTo(str1));

//        concat method()
        String str4 = "Apple";
        String str5 ="Banana";
        String str6 ="Custard Apple";
        System.out.println("concat method "+str4.concat(str5).concat(str6));
//        contains () method
//        it returns boolean value
        String str7 = new String("Have a great day!");
        System.out.println();
        System.out.println(str7.contains("great"));
        System.out.println(str7.contains("Great"));
//     endsWith() method
        System.out.println("ends with "+str7.endsWith("!"));
//        equals ()method
//        it returns boolean value
        System.out.println("equals method return "+(str2.equals(str3)));
//        getChars() method
        char[] chArr = new char[25];
        str4.getChars(0, 5, chArr,2);
        System.out.println(Arrays.toString(chArr));
//        index Of() method
        System.out.println("index of char "+str5.indexOf('a'));
        System.out.println("index of char "+str5.indexOf('a',2));
//        string intern() method
//        when string objects compared it always return false
        String str8= new String("Java");
        String str9 = new String("Java");
        System.out.println("compared between two object before intern method ");
        System.out.println(str8==str9);
        System.out.println(str8.equals(str9));
        String internStr8 =str8.intern();
        String internStr9 =str9.intern();

        System.out.println("compared between two object after intern method ");
        System.out.println(internStr8==internStr9);
//        isEmpty method
        String str10="";
        String str11 ="    ";
        System.out.println();
        System.out.println(str10.isEmpty());
        System.out.println(str11.isBlank());

// Join method()
        String str12 = String.join("-","name","giritharan","aravinth");
        System.out.println(str12);
//        equalIgnoreCase()
        String str13 ="Pasupathi";
        String str14 ="pasupathi";
        System.out.println();
        System.out.println(str13.equals(str14));
        System.out.println(str13.equalsIgnoreCase(str14));
//        split method
        String str15 = "Java,JavaScript,HTML,CSS,React,SpringBoot,MySql,Oracle";
        String[] arr15 = str15.split("a");
        System.out.println(Arrays.toString(arr15));
        for (String i:arr15){
            System.out.println(i);
        }
//        subString() method
        System.out.println("subString "+str15.substring(0,4));



        String com1 = new String("pops");
        String com2 = new String("Pops");
        //if (com1==com2){
        if (com1.equalsIgnoreCase(com2)){
            System.out.println("pops");
        }
        else {
            System.out.println("not pops");
        }

    }
}
