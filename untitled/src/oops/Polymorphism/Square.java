package oops.Polymorphism;

public class Square  extends Shape{

    @Override
    public void draw() {
        System.out.println("drawing a square shape");
    }
}
