package oops.Interface;



public class Mechanic implements Vehicle {

    @Override
    public void run() {

    }
    public void  check(Vehicle v){
        System.out.println("checking");
        v.run();
    }
    public static void main(String[] args) {
        Mechanic mechanic =new Mechanic();
        Car car =new Car();
        Bike bike =new Bike();
//     object reference created using implements
        Vehicle carObj =new Car();
        mechanic.check(car);
        mechanic.check(bike);
        mechanic.display();

    }


}
