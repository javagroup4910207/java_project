package oops.NewEncapsulation;

import oops.Encapsulation.EncapsualationClass;

public class NewEncapsulationClass extends EncapsualationClass{
    public static void main(String[] args) {
        EncapsualationClass encapsualationClass = new EncapsualationClass();
        NewEncapsulationClass newObj = new NewEncapsulationClass();
        encapsualationClass.methodOne();
//        getting protected method
        newObj.methodThree();

    }
}
