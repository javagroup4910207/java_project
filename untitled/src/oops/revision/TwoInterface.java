package oops.revision;

public interface TwoInterface {
    public void m3();
    public void m4();

    default  void m5(){
        System.out.println("method 5");
    }
}
