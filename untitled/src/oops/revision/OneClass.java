package oops.revision;

public class OneClass implements OneInterface,TwoInterface{
    @Override
    public void m1() {
        System.out.println("m1 method");
    }

    @Override
    final public void m2() {
        System.out.println("m2 method");

    }

    @Override
    public void m3() {

    }

    @Override
    public void m4() {

    }

    public static void main(String[] args) {
        OneClass obj1 = new OneClass();
        obj1.m5();
    }
}
