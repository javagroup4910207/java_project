package oops.InheritanceTwo;

import oops.inheritance.multiple.MultipleInterface;

public class SingleLevel2 extends SingleLevel1 {
    public String nativePlace;
    public int pinCode;

    public SingleLevel2() {
    }

    public SingleLevel2(String nativePlace, int pinCode) {
        this.nativePlace = nativePlace;
        this.pinCode = pinCode;
    }

    public SingleLevel2(String name, String dept, int age, String nativePlace, int pinCode) {
        super(name, dept, age);
        this.nativePlace = nativePlace;
        this.pinCode = pinCode;
    }
    public int multipleMethod(int a, int b){
        return a*b;
    }

    public  int  addMethod(int a, int b, int c, int d){
        System.out.println("child");
        return a+b+c+d;

    }
    public static  void staticMethod(){
        System.out.println("static method without before overloading");
    }
    public static void staticMethod(int a){
        System.out.println("static method overloaded");
    }


    public static void main(String[] args) {
        SingleLevel2 obj2 = new SingleLevel2();
        System.out.println( obj2.addMethod(45,55));
        System.out.println( obj2.multipleMethod(45,55));
        System.out.println(obj2.addMethod(2,4,5,3));
        MultipleInterface.staticMethod();
        staticMethod();
        staticMethod(10);
        obj2.staticMethod();

    }


}
