package oops.InheritanceTwo;

public class MultiLevel extends SingleLevel2 {
    public MultiLevel() {
    }

    public MultiLevel(String nativePlace, int pinCode) {
        super(nativePlace, pinCode);
    }

    public MultiLevel(String name, String dept, int age, String nativePlace, int pinCode) {
        super(name, dept, age, nativePlace, pinCode);
    }
    public void getAllValues() {
        System.out.println(name);
        System.out.println(dept);
        System.out.println(age);
        System.out.println(nativePlace);
        System.out.println(pinCode);
    }
//method overloading is possible in two parent class and child class
        public  String  addMethod(String a,String b,String c){
            System.out.println("parent");
            return a+b+c;


    }


    public static void main(String[] args) {
        MultiLevel obj1 =new MultiLevel("Praveen","ECE",25,"ugi",635210);
        obj1.getAllValues();


    }
}
