package oops.InheritanceTwo.MultipleInheritance;

public interface MultipleInterface {
//    its like const variable type in java script
//    once we declared  we can't reintialize the value
         public  final  int val= 100;
    public  String fn = null;
    public  String ln = null;



    public abstract  void newMethod() ;

    public abstract  void secondMethod();

    public abstract int addMethod(int num1,int num2);

    default   void defaultMethod(){
        String str1= "JackieChan";
        String str2= "jamesBond";
        System.out.println(str1+" and "+str2);
    };
//      static ---> once value modified by  object1 if we access the value by object 2  the value will be the latest value
    public  static void staticMethod(String fn,String ln){
//

        System.out.println(fn+" "+ln);
    }

}
