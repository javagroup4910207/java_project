package oops.InheritanceTwo.MultipleInheritance;


public class MultipleClass implements MultipleInterface {
    public MultipleClass() {
    }


    @Override
    public void newMethod() {
        System.out.println("New Method");
    }

    @Override
    public void secondMethod() {
        System.out.println("Second Method");

    }

    @Override
    public int addMethod(int num1, int num2) {
        return num1+num2;
    }

    @Override
    public void defaultMethod() {
        MultipleInterface.super.defaultMethod();
    }

    public static void main(String[] args) {
        MultipleClass obj1 = new MultipleClass();
        obj1.newMethod();
        obj1.defaultMethod();
        System.out.println(obj1.addMethod(val,20));
        System.out.println("dierct access"+ val);
        System.out.println("by using class "+MultipleClass.val);
        System.out.println("by object "+obj1.val);
        MultipleInterface.staticMethod("pasupathy","ganesan");


    }
}
