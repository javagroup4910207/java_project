package oops.InheritanceTwo;

public class SingleLevel1 {
    public String name;
    public String dept;
    public int age;

// Default constructors
    public SingleLevel1() {
    }

    public SingleLevel1(String name, String dept, int age) {
        this.name = name;
        this.dept = dept;
        this.age = age;
    }

    public  int  addMethod(int a, int b){
        return a+b;

    }

    public  int  addMethod(int a, int b, int c){
        return a+b+c;

    }
    public  int  addMethod(int a, int b, int c, int d){
        System.out.println("parent");
        return a+b+c+d;

    }



    public static void main(String[] args) {
        SingleLevel1 obj1  = new SingleLevel1();
        SingleLevel1 obj2 = new SingleLevel1("Pasupathy","ece",26);
        System.out.println("Name"+obj2.name);
        System.out.println( obj2.addMethod(333,444));

    }


}
