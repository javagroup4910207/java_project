package oops.Encapsulation;

public class EncapsualationClass {

      public void  methodOne(){
        System.out.println("Inside of public method");
    }
    private void methodTwo(){
        System.out.println("Inside of Private method");
    }
    protected void methodThree(){
        System.out.println("Inside of Protected method");
    }
    void  methodFour(){
        System.out.println("Inside of default method");
    }

    public static void main(String[] args) {
        EncapsualationClass  obj1 = new EncapsualationClass();
        obj1.methodTwo();

    }
}
