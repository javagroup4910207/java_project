package oops.abstraction;

public  class ImplClass extends StudentClass implements StudentInterface {

    public  void  s1(){

    }

    @Override
    public void s2() {

    }


    @Override
    public void m1() {

    }

    @Override
    public void meth(int a) {

    }

    @Override
    public void m2() {

    }
    static int m3(int a ){
        System.out.println("m3 method");
        return a;
    }

    public static void main(String[] args) {


    }
}
