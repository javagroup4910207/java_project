package oops.abstraction;

public class SecondAbstractClass  extends FirstAbstractClass {



    public static void main(String[] args) {
        SecondAbstractClass obj1 = new SecondAbstractClass();

    }

    @Override
    public void printMethod() {

    }

    @Override
    public void arithmeticMethod() {

    }

    @Override
    public void lastMethod() {

    }
}
