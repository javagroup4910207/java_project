package oops.abstraction;

public interface FirstInterface {
    public final int num=100;
    public  boolean  flag =true;
    public  abstract void printMethod();
    public abstract  void arithmeticMethod();
    public abstract  void lastMethod();
}
