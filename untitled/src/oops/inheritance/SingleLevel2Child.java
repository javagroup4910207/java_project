package oops.inheritance;

public class SingleLevel2Child extends SingleLeve1Parent {
    public int streetNO;
    public String country;
    public boolean temp;

    public SingleLevel2Child(int streetNO, String country, boolean temp) {
        this.streetNO = streetNO;
        this.country = country;
        this.temp = temp;
    }

    public SingleLevel2Child(int streetNO, String country, boolean temp, int age, String name, boolean flag) {
        super(age, name, flag);
        this.streetNO = streetNO;
        this.country = country;
        this.temp = temp;
    }

    public SingleLevel2Child() {

    }

//    public void m1(){
//        System.out.println("Inside child m2");
//    }

    public static void main(String[] args) {
        SingleLevel2Child child = new SingleLevel2Child(10, "India", false, 100, "Tamil", true );
        System.out.println(child.country);
        System.out.println("Add -> " + child.add(738,647));
        System.out.println(child.name);
        SingleLevel2Child chiObj =new SingleLevel2Child();
        chiObj.m1();

    }
}
