package oops.inheritance.multiple;

public interface MultipleInterface {

    public static final int value = 100;

    public abstract void printMethod();
    public abstract void saveMethod();

    public static void staticMethod() {
        int a =10;
        int b =40;
        System.out.println(a+b);
    }


     default void defaultMethod() {
        String a ="fhjfksjfi";
        int b =40;
        System.out.println(a+b);
    }

}
