package oops.inheritance.multiple;

public class MultipleClass implements MultipleInterface{


    @Override
    public void printMethod() {
        System.out.println("First Multiple print ");
    }

    @Override
    public void saveMethod() {
        System.out.println("First Multiple save ");
    }

    void m3(){

        System.out.println("default method");
    }

    public static void main(String[] args) {
        MultipleClass multipleClass = new MultipleClass();
        multipleClass.saveMethod();

    }
}
