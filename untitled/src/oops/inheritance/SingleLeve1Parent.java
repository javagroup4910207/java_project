package oops.inheritance;

public class SingleLeve1Parent {
    public int age;
    public String name;
    public boolean flag;

   public SingleLeve1Parent() {
    }



    public SingleLeve1Parent(int age, String name, boolean flag) {
        this.age = age;
        this.name = name;
        this.flag = flag;
    }

    public void m1(){

        System.out.println("Inside parent m1");
    }

    public int add(int a, int b){
       return  a+b;
    }

    public static void main(String[] args) {
        SingleLeve1Parent obj1=new SingleLeve1Parent();
    }

}
