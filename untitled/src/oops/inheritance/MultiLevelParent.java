package oops.inheritance;

public class MultiLevelParent extends SingleLevel2Child{

    public String pinCode;

    public MultiLevelParent(int streetNO, String country, boolean temp, String pinCode) {
        super(streetNO, country, temp);
        this.pinCode = pinCode;
    }

    public MultiLevelParent(int streetNO, String country, boolean temp, int age, String name, boolean flag, String pinCode) {
        super(streetNO, country, temp, age, name, flag);
        this.pinCode = pinCode;
    }

    public MultiLevelParent(String pinCode) {
        this.pinCode = pinCode;
    }

    public MultiLevelParent() {

    }
    public void m3(){
        MultiLevelParent obj = new MultiLevelParent();


        System.out.println("Inside multi parent m3");
    }

    public static  void staticMethod(){
        System.out.println("static methoad over ridding is possible");
    }

    public static void main(String[] args) {

        MultiLevelParent multiLevelParent = new MultiLevelParent();

        System.out.println(multiLevelParent.add(33, 8484));
        System.out.println(multiLevelParent.country);
        multiLevelParent.m3();
        multiLevelParent.m1();
    }
}
