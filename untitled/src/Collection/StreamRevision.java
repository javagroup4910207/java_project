package Collection;

import java.util.*;
import java.util.stream.Collectors;

public class StreamRevision {

    public static void main(String[] args) {
        List l = new ArrayList();

        l.add("Giri");
        l.add(45);
        l.add(45.5);
        System.out.println(l);

        List<Integer> li = new ArrayList<>();
        li.add(10);
        li.add(20);
        li.add(30);
        li.add(40);
        li.add(50);


        Integer total = li.stream().reduce(0, Integer::sum);
        System.out.println("added the list of values  "+total);


        Integer largest2Number = li.stream().sorted((a,b)->b-a).toList().get(1);


//        Integer largest2Number1 = li.stream().Collections.toList().get(1);

        System.out.println("Second largest number "+largest2Number);

        List<Integer> li1 = new ArrayList<>();
        li1.addAll(li);
        li1.add(0,100);

        List<Integer> li3 = li.stream().filter((a)->a != 100).collect(Collectors.toList());
        System.out.println("removed element "+li3);


        List<String> liStr= new ArrayList<>();
        liStr.add("jack");
        liStr.add("king");
        liStr.add("queen");
        liStr.add("ace");
        liStr.add("spade");
        liStr.add("clover");
        liStr.add("Heart");
        liStr.add("ace");
        liStr.add("ace");
        liStr.add("ace");
        liStr.add(null);
        liStr.add(null);
        liStr.add(null);
        liStr.add(null);

        List<String> liStr1= new ArrayList<>();
        liStr1.add("jack");
        liStr1.add("king");


        liStr.add(0,"aravinth");

        liStr.removeAll(liStr1);
        System.out.println(liStr);

        Iterator iterator = liStr.listIterator();

        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

        for(String i : liStr){
            System.out.println(i);
        }


        List<Integer> a;
        List<Integer> b;
        a=new ArrayList<>();
        b=new LinkedList<>();

        b.add(123);
        b.add(456);
        b.add(789);
        System.out.println(b);
        b.add(1,222);
        System.out.println(b);

            List<String> v=new Vector<String>();
            v.add("Ayush");
            v.add("Amit");
            v.add("Ashish");
            v.add("Garima");


        Stack<String> stack = new Stack<String>();

        stack.push("apple");
        stack.push("ball");
        stack.push("ball");
        stack.add("catch");
        stack.push("ball");
        stack.add("aeroplane");
        System.out.println(stack);

       Iterator iterator1 = stack.iterator();

       String st ="";
       while(iterator1.hasNext()){

           st=st+iterator1.next()+" ";
       }
        System.out.println(st);
       stack.pop();
        System.out.println("peek "+stack.peek());


//
        //   creating the object for pojo class and add into the Array list
        List<EmployeePojo> employeeList= new ArrayList<>();

        EmployeePojo obj = new EmployeePojo();
        obj.setName("giri");
        obj.setDept("mech");
        obj.setId(555);

        employeeList.add(new EmployeePojo("aravinth",123,"eie"));
        employeeList.add(obj);
        employeeList.add(new EmployeePojo());


        for(EmployeePojo i: employeeList){
            System.out.println(i.toString());
        }






//    Queue Interface


    }





}
