package Collection;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class MapLearn {
    public static void main(String[] args) {
        // key value pair
        // implementation HashMap, TreeMap, LinkedHashMap, HashTable, ConcurrentHashMap
        Map<Integer, String> firstMap = new HashMap<>();
        firstMap.put(10, "10thstandard");
        firstMap.put(11, "11thstandard");
        firstMap.put(9, "9thstandard");
        firstMap.put(5, "5thstandard");
        firstMap.put(12, "12thstandard");
        firstMap.put(null, "null value");

        firstMap.remove(12);
        System.out.println(firstMap.get(9));
        System.out.println(firstMap.getOrDefault(12, "12the removed"));
        System.out.println("Fetch key only" + firstMap.keySet());


        if (firstMap.containsKey(10)){
            System.out.println("contains key 10 -> " + firstMap.get(10));
        }

        if (firstMap.containsValue("9thstandard")){
            System.out.println("contains value 9thstandard ");
        }

        System.out.println("size -> " + firstMap.size());

        // map iterating ways
       /* for (Map.Entry<Integer, String> stringEntry : firstMap.entrySet()) {
            System.out.println(stringEntry.getKey() + " : " + stringEntry.getValue());
        }*/

        firstMap.forEach((key, value) -> System.out.println(key + " -> " + value));

     /*   firstMap.entrySet().forEach(v -> {
            System.out.println(v.getKey() + " -> " + v.getValue());
        });*/


        //Hashtable all methods are synchronized and won't allow null key
        System.out.println("Hashtable-------------------------");
        Map<Integer, String> secondMap = new Hashtable<>();
        secondMap.put(10, "10thstandard");
        secondMap.put(11, "11thstandard");
        secondMap.put(9, "9thstandard");
        secondMap.put(5, "5thstandard");
        secondMap.put(12, "12thstandard");
        //secondMap.put(null, "null value");
        secondMap.forEach((key, value) -> System.out.println(key + " -> " + value));

        // It maintains insertion order.
        System.out.println("LinkedHashMap-------------------------");
        Map<Integer, String> thirdMap = new LinkedHashMap<>();
        thirdMap.put(10, "10thstandard");
        thirdMap.put(11, "11thstandard");
        thirdMap.put(9, "Ninththstandard");
        thirdMap.put(5, "5thstandard");
        thirdMap.put(12, "12thstandard");
        thirdMap.put(null, "null value");
        thirdMap.forEach((key, value) -> System.out.println(key + " -> " + value));

        //TreeMap won't allow null key and gives the sorted value based on key
        System.out.println("TreeMap-------------------------");
        Map<Integer, String> fourthMap = new TreeMap<>();
        fourthMap.put(10, "10thstandard");
        fourthMap.put(11, "11thstandard");
        fourthMap.put(9, "9thstandard");
        fourthMap.put(5, "5thstandard");
        fourthMap.put(12, "12thstandard");
        //fourthMap.put(null, "null value");
        fourthMap.forEach((key, value) -> System.out.println(key + " -> " + value));


        //ConcurrentHashMap won't allow null key and fix the concurrent modification exception
        System.out.println("ConcurrentHashMap-------------------------");
        Map<Integer, String> fifthMap = new ConcurrentHashMap<>();
        fifthMap.put(10, "10thstandard");
        fifthMap.put(11, "11thstandard");
        fifthMap.put(9, "9thstandard");
        fifthMap.put(5, "5thstandard");
        fifthMap.put(12, "12thstandard");
        //fifthMap.put(null, "null value");
        fifthMap.forEach((key, value) -> System.out.println(key + " -> " + value));

//        Interview Questions
        Map<Integer,String> mp = new HashMap<>();
        mp.put(1,"apple");
        mp.put(2,"ball");
        mp.put(3,"cat");

        List<String> li =  mp.entrySet().stream().filter((a)-> a.getValue().startsWith("a")).map((m)-> m.getValue()).collect(Collectors.toList());

        System.out.println(li);

        List<EPojo> la = new ArrayList<>();

        la.add(new EPojo("ab","xyz") );
        la.add(new EPojo("cd","xyz") );
        la.add(new EPojo("ef","xyz") );
        la.add(new EPojo("pq","xyz") );
        la.add(new EPojo("gh","xyz") );

        long a  = la.stream().filter((b) -> !Objects.equals(b.name, "pq")).count();
        System.out.println(a);

    }
}
