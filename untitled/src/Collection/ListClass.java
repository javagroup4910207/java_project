package Collection;

import pojo.EmployeePojo;

import java.util.*;

public class ListClass {

    public void m1(){
    }

    public static void main(String[] args) {
        String name ="xxx";
//        traditional array
        String[] cricket = new String[10];
        cricket[0]= "bat";
        cricket[1] ="ball";
        cricket[cricket.length - 1] = "stump";
//        collection
//        ArrayList
        ArrayList<Object> arr1 = new ArrayList<>();
        arr1.add("giri");
        arr1.add(5);
        arr1.add(10.4);
        arr1.add(true);
        System.out.println(arr1);
        System.out.println("length of the elements are "+arr1.size());
//        in above arrayList we can add different datatype value
//        to overcome this pass wrapper class (boxing and unboxing)
        ArrayList<Integer> arr2 = new ArrayList<>();
        arr2.add(5);
        for(int i=1;i<=10;i++){
            arr2.add(i*10);
        }
        System.out.println(arr2);

//        array list is flexible and it allows data redundancy
//        to iterate ArrayList using forEach method
        for (int i: arr2){
            System.out.println(i);
        }
//  to iterate a ArrayList using Iterator method
//        creating  arrayList object to the IteratorClass
//
        Iterator <Integer> arr2Itertor = arr2.iterator();
        System.out.println("By using Iterator");

        while (arr2Itertor.hasNext()){
            System.out.println(arr2Itertor.next());

        }
// string based list
        ArrayList<String> sports= new ArrayList<>();
        sports.add("Cricket");
        sports.add("VolleyBall");
        sports.add("Kabadi");
        sports.add("Football");
        sports.add("Hockey");
        sports.add("Badminton");
        System.out.println("Sports.............!");

        ArrayList<String> sportsNew= new ArrayList<>();
//
//        addAll method
        sportsNew.addAll(sports);
        System.out.println("sports "+sports);
        System.out.println("sportsNew "+sportsNew);

        System.out.println(sports.equals(sportsNew));
        System.out.println(sports==sportsNew);

//cloneMethod clone()
        List<String> sportsClone=(ArrayList<String>) sports.clone();
        List<String> sportsClone1=(ArrayList<String>) sports.clone();
//        contains method
        if(sports.contains("Hockey")){
            System.out.println("yes Hockey is contain in sports");
        }
//        containsAll method
//        System.out.println(sports.);

        System.out.println("employee Pojo");

        List<EmployeePojo> employeePojos = new ArrayList<>();
        employeePojos.add(new EmployeePojo("Apple", 12, 45, "6474748483", null));
        employeePojos.add(new EmployeePojo("orange", 73, 45, "6474748483", null));
        employeePojos.add(new EmployeePojo("Grapes", 63, 45, "6474748483", null));

        List<EmployeePojo> employeePojos1 = new ArrayList<>();
        employeePojos1.add(new EmployeePojo("Mango", 12, 45, "6474748483", null));
        employeePojos1.add(new EmployeePojo("Jack", 73, 45, "6474748483", null));
        employeePojos1.add(new EmployeePojo("Star", 63, 45, "6474748483", null));

        employeePojos.addAll(employeePojos1);

        System.out.println(employeePojos);

        ListIterator<EmployeePojo> listIterator = employeePojos.listIterator();

        while (listIterator.hasNext()){
            System.out.println(listIterator.next());
        }
//
        List<java.io.Serializable> list1=new ArrayList<java.io.Serializable>();
        System.out.println("boolean value "+list1.isEmpty());

        list1.add(1);
        list1.add("advento");
        list1.add(45.5);
        list1.add(45.5);
        list1.add(45.5);
        list1.add(13);
        list1.add(14);
        list1.add(15);
        list1.add(16);
        list1.add(1,100);
        list1.add(list1.size(),90);

        System.out.println("contains all method "+list1.containsAll(Arrays.asList(0,13,14,15)));



        list1.set(4,200);

        System.out.println(list1);
//       List li=  list1.stream().sorted().collect(Collectors.toList());


        System.out.println(list1.get(1));
        list1.set(0,1000);
        list1.remove(2);
        System.out.println("boolean value "+list1.isEmpty());
        System.out.println("lastindex of "+list1.lastIndexOf(1000));
        System.out.println("contains "+list1.contains(1000));

//
        List list1Remove=new ArrayList<>();
        list1Remove.add(13);
        list1Remove.add(14);
        list1Remove.add(15);
        list1Remove.add(16);
        list1.removeAll(list1Remove);
        System.out.println("inndex "+list1.indexOf("advento"));
        System.out.println("size "+list1.size());





//        list1.clear();
        System.out.println(list1);
                ArrayList<Integer> list2=new ArrayList<>();
             List<Double> list3;
             list3= new LinkedList<>();



    }
}
