package Collection;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class MapExercise {
    public MapExercise() {
    }

    public static void hashMapMethod() {
        Map<String, Integer> marks = new HashMap<>();
        marks.put("tamil", 98);
        marks.put("english", 98);
        marks.put("maths", 90);
        marks.put("social", 98);
        marks.put("science", 100);
        marks.put("science", 120);
        marks.put("sc", 120);
        marks.put(null,50);

        System.out.println("size of hash map "+marks.size());


        if(marks.containsValue(100)){
            System.out.println("value is present");
        }
        System.out.println(marks.keySet());
        System.out.println(marks.values());
        System.out.println("hashCode"+marks.hashCode());
        System.out.println("hashCode"+marks.hashCode());

        for(Map.Entry<String, Integer> obj:marks.entrySet()){
            System.out.println(obj.getKey()+"="+obj.getValue());
        }


    }

    public static void main(String[] args) {
        hashMapMethod();

//        Map<String,String> map = new ConcurrentHashMap<>();

    }

}
