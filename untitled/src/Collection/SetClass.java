package Collection;

import java.util.*;

public class SetClass {
    public static void main(String[] args) {
        Set<String> stringSet = new HashSet<>();
        stringSet.add("Apple");
        stringSet.add("Orange");
        stringSet.add("Grapes");
        stringSet.add("Apple");
        stringSet.add("Jack Fruit");
        stringSet.add("Strawberry");
        stringSet.add("Apple");




        System.out.println(stringSet);

        Set<String> stringSet1 = new LinkedHashSet<>();
        //stringSet1.addAll(stringSet);
        stringSet1.add("Apple");
        stringSet1.add("Orange");
        stringSet1.add("Grapes");
        stringSet1.add("Apple");
        stringSet1.add("Jack Fruit");
        stringSet1.add("Strawberry");
        stringSet1.add("Apple");

        System.out.println("stringSet1 " + stringSet1);

        Iterator<String> arr2Itertor = stringSet1.iterator();
        System.out.println("By using Iterator");

        while (arr2Itertor.hasNext()) {
            System.out.println(arr2Itertor.next());
        }

        //SortedSet<Integer> tree = new
        // <>();
        Set<Integer> tree = new TreeSet<>();
        tree.add(12);
        tree.add(346);
        tree.add(647);
        tree.add(12);
        tree.add(53);

        System.out.println("treeSet " + tree);
    }
}
