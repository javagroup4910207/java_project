import java.util.*;
import java.util.stream.Collectors;

public class
ArrayExercise {
    String name = "xxxx";

    public static  String  star = "* ";
    public  static void pyramidPtn(int args) {

        for(int i=0; i<args;i++){
            String out ="";

            for(int j=0;j<args-i;j++){
                out+= " ";

            }
            for(int k=0;k<=i;k++){
                out+=star;

            }
            System.out.println(out);

        }

    }
    public  static void sortArr(){

        int newArr[] ={10,30,40,50,35,70,45};
        for(int i=0;i<newArr.length;i++){
            for(int j=i+1;j<newArr.length;j++ ){
                if(newArr[i]>newArr[j]){
                    newArr[i]=newArr[j]+newArr[i];
                    newArr[j]=newArr[i]-newArr[j];
                    newArr[i]=newArr[i]-newArr[j];
                }

            }


        }
        System.out.println(Arrays.toString(newArr));
    }
    public static  void uniqueValueMethod(){
       int  dupArr[] ={10,10,30,45,90,45,50};
       int dupArr2[]={0};

       for(int i=0; i<dupArr.length-1;i++){
           for(int j=0;j<dupArr2.length-1;j++){
               if (dupArr[i] != dupArr2[j]){
                   dupArr2[dupArr2.length]=dupArr[i];
                   System.out.println(dupArr[i]);
               }

           }
       }
        System.out.println(Arrays.toString(dupArr2));
    }

    public static void main(String[] args) {
        String [] dummy =new String [20];
        char[] ch = new char[10];
        int[] dummy2 =new  int[34];
        int sizeArr[] = new int[10];
        sizeArr[0]=1;

        sizeArr[1]=2;
        int newArr[] = new int[]{2000,800,1500,1400};
        newArr[3] = 900;

        List lis = Arrays.asList(Arrays.toString(newArr));
        System.out.println(lis.toString());

        System.out.println(Arrays.toString((newArr)));
        System.out.println("stream");
        Arrays.stream(newArr).sorted().forEach((a)-> System.out.print(a +" "));
        System.out.println("dn gr");
        Arrays.stream(newArr).sorted().boxed().forEach((a)-> System.out.print(a +" "));
        Arrays.stream(newArr).sorted().boxed().forEach((a)-> System.out.print(a +" "));
        System.out.println("stream");
//        print array without loop
        System.out.println(Arrays.toString(sizeArr));


        int numArr[] ={10,20,30,40,50,60,70,80};
        int n[] = new int[29];
        for(int i=0; i<numArr.length; i++){
            System.out.print(numArr[i]+" ");
        }
        System.out.println();
//        Pyramid Pattern
        pyramidPtn(5);
//        sorting  the number
        sortArr();

        uniqueValueMethod();

//        Remove Duplicates
        Integer[] arrayWithDuplicates = {1, 2, 3, 4, 2, 3, 5};
//        convert array into list

        List<Integer> lis1  = Arrays.asList(arrayWithDuplicates);
        System.out.println(lis1);

        // Convert array to set to remove duplicates
        Set<Integer> setWithoutDuplicates = new HashSet<>(Arrays.asList(arrayWithDuplicates));

        // Convert set back to array (if needed)
        Integer[] arrayWithoutDuplicates = setWithoutDuplicates.toArray(new Integer[0]);
        Object[] arrayWithoutDuplicates1 = setWithoutDuplicates.toArray();

        // Print array without duplicates
        System.out.println(Arrays.toString(arrayWithoutDuplicates));

//        convert the array into array list
        int[] arr = {29,45,66,77,77,75,55};

        List<Integer> arLi = Arrays.stream(arr).boxed().collect(Collectors.toList());
        System.out.println("arLi "+arLi);

    }

}
