import java.sql.SQLOutput;

public class Main {
    //    Access Specifiers
    public static void newMain() {
        System.out.println("public: Accessible in all classes in your application.");

    }
    private static void pvtMethod(){
        System.out.println("private: Accessible only within the class in which it is defined.");
        System.out.println("-----------------------");
    }

    protected static  void protectedMethod(){
        System.out.println("protected: Accessible within the package in which it is defined and in its subclass(es) (including subclasses declared outside the package).");
        System.out.println("-----------------------");
    }
    static  void defaultMethod (){
        System.out.println("default (declared/defined without using any modifier): Accessible within the same class and package within which its class is defined.");
    }
// non-static method
    public static void nonStaticMethod(){
        System.out.println("inside of Non-static method");
    }
    public static void main(String[] args) {
        newMain();
        System.out.println("Hello world!");
        pvtMethod();
        protectedMethod();
        defaultMethod();
        Main object1 = new Main();
       object1.nonStaticMethod();


    }

}










