package StringBufferAndBuilder;

public class BufferClass {
    public static void main(String[] args) {

        StringBuffer stringBuffer = new StringBuffer("Apple ");
        stringBuffer.append("Orange ");
        stringBuffer.append("Pine_Apple ");
//converting primitive to String
        int a =10;
        String ss = String.valueOf(20);
        String sr = a+"";
        String str = new String(String.valueOf(a));

        String ref = stringBuffer.toString() ;
        System.out.println(ref);
        System.out.println(stringBuffer.reverse());

        stringBuffer.insert(10, " -Insert- ");
        System.out.println(stringBuffer);

        stringBuffer.delete(10, 15);
        System.out.println(stringBuffer);

        stringBuffer.replace(10,15,"/");

        stringBuffer.reverse();

        System.out.println(stringBuffer);

        System.out.println(stringBuffer.substring(0,15));
        System.out.println(stringBuffer.substring(15));

        System.out.println(stringBuffer.length());

       char ch= stringBuffer.charAt(10);
        System.out.println(ch);

        int a1 = stringBuffer.indexOf("A", 10);
        System.out.println(a);

        int b = stringBuffer.indexOf("A");
        System.out.println(b);
    }
}
