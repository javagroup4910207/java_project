import java.util.Scanner;
public class Datatype {
     String var = "global variable";
    static int num ;

    private int newValue =500;
    public Datatype (){

    }
    public Datatype (String var){
        this.var =var;

    }
   public Datatype(int num) {
       this.num = num;
   }
    public Datatype(String var,int num) {
        this.num = num;
        this.var =var;
    }

    public final void meth(){

    }
    public  int meth(int a){

           return 1;
    }

//GET & SET METHOD
    public  String getVar() {
        return this.var;
    }

    public  void setVar(String var) {
        this.var = var;
    }

    public static int getNum() {
        return num;
    }

    public static void setNum(int num) {
        Datatype.num = num;
    }

    public int getNewValue() {
        return newValue;
    }

    public void setNewValue(int newValue) {
        this.newValue = newValue;
    }

    public static void main(String[] args){
//        Primitive Datatype
        String newChar ="New String";
        int num =10;
        int num2 = 50000;
        String singleChar="A";
        char ascii = 65;
        float val = 10.5F;
        int maxNumber = 2147483647;
        long longNumber=2147483648l;
        byte byteNumber = 127;
        byte nByte = 45;
        int intNumber = 128;
//        to count the zero we can add _ like this
        int countZero=10_0_0_00;

        double val2=20.5;
        boolean bool =true;

//        float to int conversion;
        //        explicit
        int a = (int) val;
        int numOne =0x100;
        System.out.println(numOne);

//implicit
        float flNum=10.38f;

        int b=a;
//        narrow casting
//        widening casting
//        promote


        System.out.println("Hello Giri");
        System.out.println("ascii of 65 "+ascii);

//    default constructor
        Datatype object = new Datatype();
        System.out.println(object.var);
        System.out.println(object.var);

//        constructor  with one argument
        Datatype object1 = new Datatype("jjdj");
        System.out.println(object1.var);

//      constructor with one integer argument

        Datatype object2 = new Datatype(45);
//        object2.num =45;

        System.out.println(object2.num);
        System.out.println("object2 "+object2.var);
        System.out.println("object1 "+object1.num);
//      constructor with one integer argument and one string
        Datatype object3 = new Datatype("aravinth",50);
        System.out.println(object3.var+" and " + object3.num);
//
        Datatype objectFour = new Datatype();
        objectFour.setNum(100);
        objectFour.setNewValue(10);
        System.out.println("new variable value "+ objectFour.getNewValue() );
        System.out.println("from getter method num = " + getNum());

        Datatype objectFive = new Datatype();

        System.out.println("from getter method num = " + objectFive.getNum());
        System.out.println("new variable value "+ objectFive.getNewValue() );
//        check address of the value
        String  love ="Valentine's Day";
        String  failure ="Valentine's Day";

        System.out.println("love"+ love.hashCode());
        System.out.println("failure"+ failure.hashCode());
             love=love+" feb 14";
        System.out.println(love);
        System.out.println("love"+ love.hashCode());
        int[] arr = {1,2,3,4,5,6,8};
        System.out.println("arr"+arr.hashCode());
        arr[0] = 90;
        System.out.println("arr"+arr.hashCode());

//







    }
}
