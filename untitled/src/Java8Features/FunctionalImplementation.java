package Java8Features;

public class FunctionalImplementation {
    int num;

    private FunctionalImplementation(int num) {
        this.num = num;
    }

    private FunctionalImplementation() {
    }

    public void m3 (){
        System.out.println("m3");
    }


    public static void main(String[] args) {
        FunctionalImplementation obj = new FunctionalImplementation(10);
        FunctionalImplementation obj1 = new FunctionalImplementation(20);
        FunctionalImplementation obj2 = new FunctionalImplementation(35);
        obj1.m3();
        obj.m3();
        obj2.m3();
        System.out.println("obj "+obj.num);
        System.out.println("obj1 "+obj1.num);
        System.out.println("obj "+obj.num);
        System.out.println("obj2 "+obj2.num);
        System.out.println("obj "+obj.num);
//implemented functional interface using lambda expression
        FunctionalInterfaceEx functionalInterfaceEx = (int a, int b) -> {
            System.out.println(a + b);
        };

        functionalInterfaceEx.m1(12,64);

        functionalInterfaceEx.m2("giri","ari");

    }
}
