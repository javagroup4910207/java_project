package Java8Features;

public class MethodRefernceOne {
    public  void  inMethod(){
        System.out.println("instance method");
    }
    public static  void stMethod(){
        System.out.println("static method");
    }


    public static void main(String[] args)  {

//implemented functional interface using lambda expression
        MethodRefernceOneInterface refernceOneInterface = ()->{
            System.out.println("firstMethod");
        };
        refernceOneInterface.method1();
//
        MethodRefernceOne obj = new MethodRefernceOne();
//        implemented functional interface using  methodrefernce---instance refernce;
        MethodRefernceOneInterface refernceOneInterface1=obj::inMethod;

        refernceOneInterface1.method1();

//        implemented functional interface using   methodrefernce---static reference;
        MethodRefernceOneInterface refernceOneInterface2=MethodRefernceOne::stMethod;
        refernceOneInterface2.method1();

    }
}
