package Java8Features;

import java.security.PublicKey;

public class SingletonClass {

    private static SingletonClass singletonClass;

    private SingletonClass() {

    }

    public static SingletonClass getSingletonClass() {
        if(singletonClass == null) {
            singletonClass = new SingletonClass();
        }
        return  singletonClass;
    }
    public void m1 (){
        System.out.println("m1 method ");
    }

    public static void main(String[] args) {


    }
}
