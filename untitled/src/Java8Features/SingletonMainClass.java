package Java8Features;



public class SingletonMainClass {
    public static void main(String[] args) {

        SingletonClass singletonClass2 = SingletonClass.getSingletonClass();
        SingletonClass singletonClass3 = SingletonClass.getSingletonClass();

        System.out.println(singletonClass2 == singletonClass2);
//        we can't create singleton class more than one object;
//        SingletonClass singletonClass4 = new SingletonClass();



    }
}
