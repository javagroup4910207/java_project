package Java8Features;

@FunctionalInterface
public interface FunctionalInterfaceEx {
    public abstract void m1(int a, int b);

    public default  void m2(String a, String b){
        System.out.println(a+" "+b);
    }


}
