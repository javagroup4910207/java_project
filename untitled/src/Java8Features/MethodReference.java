package Java8Features;

import java.util.ArrayList;
import java.util.List;

interface JavaInterface{
    void showInterfaceInfo();
}
public class MethodReference {
    void show() {
        System.out.println("This is called using instance method reference");
    }

    void shows() {
        System.out.println("This is called using instance----- method reference");
    }
    public static void main(String[] args) {
        MethodReference mr = new MethodReference();
        JavaInterface ji = mr::shows;
        ji.showInterfaceInfo();

        List<Integer> arr = new ArrayList<>();
        arr.add(5);
        arr.add(13);
        arr.add(14);
        arr.add(12);
        arr.add(56);
        arr.add(12);

        arr.forEach(System.out::println);
    }
}
