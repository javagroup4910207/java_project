package pojo;

public class EmployeePojo {
    private String name;
    private int empId;
    private int age;
    private String Phone;
   private  AddressPojo addressPojo;

    public EmployeePojo() {
    }

    public EmployeePojo(String name, int empId, int age, String phone, AddressPojo addressPojo) {
        this.name = name;
        this.empId = empId;
        this.age = age;
        Phone = phone;
        this.addressPojo = addressPojo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public AddressPojo getAddressPojo() {
        return addressPojo;
    }

    public void setAddressPojo(AddressPojo addressPojo) {
        this.addressPojo = addressPojo;
    }

    @Override
    public String toString() {
        return "EmployeePojo{" +
                "name='" + name + '\'' +
                ", empId=" + empId +
                ", age=" + age +
                ", Phone='" + Phone + '\'' +
                ", addressPojo=" + addressPojo +
                '}';
    }
}

