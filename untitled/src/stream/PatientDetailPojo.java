package stream;

public class PatientDetailPojo {
    private String name;
    private String bloodGroup;
    private String phoneNo;
    private int age;

    public PatientDetailPojo(String name, String bloodGroup, String phoneNo, int age) {
        this.name = name;
        this.bloodGroup = bloodGroup;
        this.phoneNo = phoneNo;
        this.age = age;
    }

    public PatientDetailPojo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "PatientDetailPojo{" +
                "name='" + name + '\'' +
                ", bloodGroup='" + bloodGroup + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", age=" + age +
                '}';
    }
}




