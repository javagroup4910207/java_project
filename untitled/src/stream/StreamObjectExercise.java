package stream;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamObjectExercise {
    public static void main(String[] args) {

        List<PatientDetailPojo> patientList = new ArrayList<>();



        patientList.add(new PatientDetailPojo("Zebra","A+","98587457",32) );
        patientList.add(new PatientDetailPojo("dog","o+","98587458748",54) );
        patientList.add(new PatientDetailPojo("cat","A+","98587458748",21) );
        patientList.add(new PatientDetailPojo("mouse","o-","98587458748",15) );
        patientList.add(new PatientDetailPojo("rabbit","A1+","98587458748",45) );
        patientList.add(new PatientDetailPojo("squirrel","B+","98587458748",47) );
        patientList.add(new PatientDetailPojo("monkey","B+","98587458748",60) );
        patientList.add(new PatientDetailPojo("hyena","B+","98587458748",77) );
        patientList.add(new PatientDetailPojo("kangaroo","B+","98587458748",67) );


        List<PatientDetailPojo> collectionList= patientList.stream().sorted((value1, value2) -> {
           return value1.getName().compareToIgnoreCase(value2.getName());
       }).toList();
        List<PatientDetailPojo> collectionListbyAge= patientList.stream().sorted((value1, value2) -> {
            return value1.getAge()-(value2.getAge());
        }).collect(Collectors.toList());

        collectionListbyAge.forEach(p->{
            System.out.println(p);
        });
//     Advanced methods
//        collectionList.forEach(System.out::println);

//        filter by age
        System.out.println("aged person list");
        List<PatientDetailPojo> filterByAge = patientList.stream().filter(a -> a.getAge() >= 60).toList();

        filterByAge.forEach(p->{
            System.out.println(p);
        });

        Integer integer = new Integer(10);

        System.out.println(integer);

    }
}
