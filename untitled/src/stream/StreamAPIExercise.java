package stream;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamAPIExercise {

    public static boolean cal(int num) {
        boolean boo = false;
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                boo = false;
                break;
            } else {
                boo = true;
            }
        }
        return boo;
    }


    public static void main(String[] args) {

//      find   prime number using stream
        List<Integer> li3 = Arrays.asList(45, 49, 7, 5, 3, 91, 23, 7).stream().filter(StreamAPIExercise::cal).collect(Collectors.toList());
        System.out.println(li3.toString());

//        array int stream basics
        int arr[] = {10, 45, 667, 12, 45, 56, 78, 12, 34, 10, 23, 12};
        Stream<Integer> integerStream = Arrays.stream(arr).boxed().sorted();


        integerStream.forEach(a -> System.out.print(a + " "));
        Stream<Integer> distinct = Arrays.stream(arr).boxed().distinct();
        distinct.forEach(System.out::println);

        List<Integer> integerList = new ArrayList<>();
        integerList.add(10);
        integerList.add(20);
        integerList.add(10);
        integerList.add(40);
        integerList.add(25);
        integerList.add(20);
        integerList.add(35);

        System.out.println("distinct values");
        integerList.stream().distinct().forEach((o) -> {
            System.out.print(o + " ");

        });

        integerList.stream().filter((a) -> a < 10).collect(Collectors.toList());
        Collections.sort(integerList);
        System.out.println(integerList.toString());

        integerList.stream().distinct().forEach(System.out::println);

        System.out.println("count " + integerList.stream().count());
        integerList.stream().sorted().forEach(System.out::print);
        System.out.println();
        integerList.stream().sorted(Collections.reverseOrder()).forEach(a -> {
            System.out.print(a + ",");
        });
        System.out.println();
//
        List<Integer> collect = integerList.stream().filter(e -> e > 20).collect(Collectors.toList());
        System.out.println("for each method");
        integerList.stream().filter(e -> e > 20).forEach(System.out::println);
        System.out.println();
        System.out.println("collect" + collect);
        Iterator<Integer> integerIterator = collect.listIterator();
        while (integerIterator.hasNext()) {
            System.out.print(integerIterator.next() + " ");
        }
        System.out.println();
        for (Integer i : collect) {
            System.out.println("iterate" + i);
        }
        System.out.println("for each method1");
        collect.forEach(a -> {
            System.out.println(a);
        });
        System.out.println("for each method2");
        collect.forEach(System.out::println);
//        print even number using stream method

        IntStream.rangeClosed(0, 100).filter(a -> a % 2 == 0).forEach(System.out::print);
        System.out.println();
        IntStream.rangeClosed(0, 100).map(a -> a + 11).filter(a -> a % 3 == 0).forEach(b -> System.out.print(b + " "));


        IntStream.of(10, 33, 200).filter(a -> a % 2 == 0).forEach(System.out::println);

//        FlatMap Method

        List<List<Integer>> li = Arrays.asList(Arrays.asList(10, 12, 12, 34, 12, 45), Arrays.asList(10, 567, 787));

        List<Integer> li1 = li.stream().flatMap(List::stream).collect(Collectors.toList());

        List<Integer> li2 = Arrays.asList(10, 322, 32, 32, 45, 67, 89, 90).stream().filter((a) -> a % 2 == 0).collect(Collectors.toList());

        System.out.println(li2.toString());



    }
}

